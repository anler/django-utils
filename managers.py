from django.db import models


class StringMatchingManager(models.Manager):
    """Manager para hacer consultas de string matching
    ej:
        Busqueda fulltext:

            Model.string_matching.match_against(fields=['name'], values=['malcom'])

        Busqueda con string matching aproximado:

            Model.string_matching.levenshtein_distance(field='name', value='malocm')
    """
    def levenshtein_distance(self, field, value, start=0, end=4):
        table = "`%s`.`%s`" % (self.model._meta.db_table, field)
        between = "between %d and %d" % (start, end)
        conditions = "levenshtein(%s, " + table + ") " + between

        select = {'distance': 'levenshtein(%%s, %s)' % table}
        return self.extra(select=select, where=[conditions], order_by=['distance'], params=(value,), select_params=(value,))

    def match_against(self, fields, values):
        match = 'MATCH(%s)' % ', '.join(['`%s`' % f for f in fields])
        against = 'AGAINST(%s)'

        conditions = ' '.join([match, against])

        select = {'relevance': conditions}
        return self.extra(select=select, where=[conditions], params=values, select_params=values)


class MySQLOptimizedManager(models.Manager):
    """Counting all rows is very expensive on large Innodb tables. 
    This manager uses a mysql-optimized Query object that returns
    an approximation if count() is called with no additional constraints.
    As I said, this is MySQL specific.
    """ 
    def get_query_set(self):
        return QuerySet(self.model, using=self._db, query=sql.MySQLOptimizedQuery(self.model))


